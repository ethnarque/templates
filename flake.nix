{
  description = "ethnarque's flake templates";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };

  outputs = { self, nixpkgs }:
    let
      systems = [ "x86_64-linux" "i686-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];

      forAllSystems = fn:
        (nixpkgs.lib.genAttrs systems
          (system: fn
            (
              let
                pkgs = import nixpkgs {
                  inherit system;
                  overlays = [ ];
                };
              in
              { inherit pkgs system; }
            )
          ));
    in
    {
      formatter = forAllSystems ({ pkgs, ... }: pkgs.nixpkgs-fmt);

      templates = {
        c-hello = {
          path = ./c-hello;
          description = "Simple Hello world in C";
        };

        laravel-hello = {
          path = ./laravel-hello;
          description = "Simple Hello world in Laravel";
        };

        ocaml-hello = {
          path = ./ocaml-hello;
          description = "Simple Hello world in OCaml";
        };

        symfony-hello = {
          path = ./symfony-hello;
          description = "Simple Hello world in Symfony";
        };

        vala-hello = {
          path = ./vala-hello;
          description = "Simple Hello world in Vala";
        };

        wordpress-hello = {
          path = ./wordpress-hello;
          description = "Simple Hello world in WordPress";
        };
      };
    };
}
