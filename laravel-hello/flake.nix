{
  description = "Simple hello world using Laravel";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    devenv.url = "github:cachix/devenv";
    devenv.inputs.nixpkgs.follows = "nixpkgs";
  };

  nixConfig = {
    extra-trusted-public-keys = "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw=";
    extra-substituters = "https://devenv.cachix.org";
  };

  outputs = { self, devenv, nixpkgs } @ inputs:
    let
      systems = [ "x86_64-linux" "i686-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];

      forEachSystems = fn:
        (nixpkgs.lib.genAttrs systems
          (system: fn
            (
              let
                pkgs = import nixpkgs {
                  inherit system;
                  overlays = [ ];
                };
              in
              { inherit pkgs system; }
            )
          ));
    in
    {
      packages = forEachSystems ({ system, ... }: {
        devenv-up = self.devShells.${system}.default.config.procfileScript;
      });

      devShells = forEachSystems ({ pkgs, ... }: {
        default = devenv.lib.mkShell {
          inherit inputs pkgs;

          modules = [
            ({ pkgs, ... }: {
              name = "laravel-hello";

              packages = with pkgs;[
                phpactor
                php82Packages.composer
              ];

              languages.javascript.enable = true;
              languages.javascript.package = pkgs.nodejs;

              languages.php.enable = true;

              services.postgres.enable = true;
              services.postgres.initialDatabases = [
                { name = "laravel"; }
              ];
              services.postgres.listen_addresses = "127.0.0.1";
              services.postgres.initialScript = ''
                CREATE ROLE postgres SUPERUSER;
                CREATE ROLE laravel;
              '';

              processes.artisan-serve.exec = "${pkgs.php}/bin/php artisan serve";
              processes.vite-dev.exec = "${pkgs.nodejs}/bin/npm run dev";
            })
          ];
        };
      });

      formatter = forEachSystems ({ pkgs, ... }: pkgs.nixpkgs-fmt);
    };
}
