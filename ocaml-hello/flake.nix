{
  description = "Simple Hello world with OCaml";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };

  outputs = inputs @ { self, nixpkgs, ... }:
    let
      package = "app";

      systems = [ "x86_64-linux" "i686-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];

      forAllSystems = fn:
        (nixpkgs.lib.genAttrs systems (
          system:
          fn (
            let
              pkgs = nixpkgs.legacyPackages.${system};
            in
            { inherit pkgs system; }
          )
        ));
    in
    {
      packages = forAllSystems ({ pkgs, system }: {
        default = self.packages.${system}.app;

        app = pkgs.ocamlPackages.buildDunePackage {
          pname = "${package}";
          version = "0.1.0";

          src = ./.;
        };
      });

      apps = forAllSystems ({ pkgs, system }: {
        default = self.apps.${system}.app;

        app = {
          type = "app";
          program = "${self.packages.${system}.app}/bin/${package}";
        };
      });

      devShells = forAllSystems ({ pkgs, ... }: {
        default = pkgs.mkShell {
          nativeBuildInputs = with pkgs;[
            dune_3
            ocaml
          ];

          buildInputs = with pkgs;[
            ocamlPackages.findlib
            ocamlPackages.utop
            ocamlPackages.odoc
            ocamlPackages.base
          ];

          packages = with pkgs; [
            nil
            ocamlPackages.ocaml-lsp
            ocamlPackages.ocamlformat
          ];
        };
      });

      formatter = forAllSystems ({ pkgs, ... }: pkgs.nixpkgs-fmt);
    };
}
