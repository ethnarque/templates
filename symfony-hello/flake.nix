{
  description = "Simple hello world using Symfony";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    devenv.url = "github:cachix/devenv";
    devenv.inputs.nixpkgs.follows = "nixpkgs";
  };

  nixConfig = {
    extra-trusted-public-keys = "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw=";
    extra-substituters = "https://devenv.cachix.org";
  };

  outputs = { self, devenv, nixpkgs } @ inputs:
    let
      systems = [ "x86_64-linux" "i686-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];

      forEachSystems = fn:
        (nixpkgs.lib.genAttrs systems
          (system: fn
            (
              let
                pkgs = import nixpkgs {
                  inherit system;
                  overlays = [ ];
                };
              in
              { inherit pkgs system; }
            )
          ));
    in
    {
      packages = forEachSystems ({ system, ... }: {
        devenv-up = self.devShells.${system}.default.config.procfileScript;
      });

      devShells = forEachSystems ({ pkgs, ... }: {
        default = devenv.lib.mkShell {
          inherit inputs pkgs;

          modules = [
            ({ pkgs, ... }: {
              name = "symfony-hello";

              packages = with pkgs;[
                nil
                symfony-cli
                phpactor
                php82Packages.composer
              ];

              languages.php.enable = true;

              services.postgres.enable = true;
              services.postgres.initialDatabases = [
                { name = "symfony"; }
              ];
              services.postgres.listen_addresses = "127.0.0.1";
              services.postgres.initialScript = ''
                CREATE ROLE postgres SUPERUSER;
                CREATE ROLE symfony WITH ENCRYPTED PASSWORD 'password';
                ALTER ROLE "symfony" WITH LOGIN;
              '';

              enterShell = ''
                eval "$(`pwd`/bin/console completion $""{0:5})"
              '';

              processes.symfony-start.exec = "${pkgs.symfony-cli}/bin/symfony server:start";
            })
          ];
        };
      });

      formatter = forEachSystems ({ pkgs, ... }: pkgs.nixpkgs-fmt);
    };
}
