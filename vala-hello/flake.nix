{
  description = "Simple Hello world in Vala";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };

  outputs = { self, nixpkgs }:
    let
      systems = [ "x86_64-linux" "i686-linux" "aarch64-linux" ];

      forAllSystems = fn:
        (nixpkgs.lib.genAttrs systems
          (system: fn
            (
              let
                pkgs = import nixpkgs {
                  inherit system;
                  overlays = [ ];
                };
              in
              { inherit pkgs system; }
            )
          ));
    in
    {
      formatter = forAllSystems ({ pkgs, ... }: pkgs.nixpkgs-fmt);

      packages = forAllSystems ({ pkgs, system }: {
        default = self.packages.${system}.vala-hello;

        vala-hello = pkgs.stdenv.mkDerivation {
          pname = "vala-hello";
          version = "0.1";
          src = self;

          doCheck = true;

          buildInputs = with pkgs;[
            glib
            gtk4
          ];

          nativeBuildInputs = with pkgs; [
            meson
            ninja
            vala
            gobject-introspection
            pkg-config
          ];


          meta = {
            description = "Vala starter template";
            platforms = pkgs.lib.platforms.linux;
          };

        };
      });


      apps = forAllSystems ({ pkgs, system }: {
        default = self.apps.${system}.vala-hello;

        vala-hello = {
          type = "app";
          program = "${self.packages.${system}.vala-hello}/bin/vala-hello";
        };
      });

      devShells = forAllSystems ({ pkgs, system }: {
        default = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            meson
            ninja
            vala
            gobject-introspection
            pkg-config
          ];

          buildInputs = with pkgs;[
            glib
            gtk4
          ];

          packages = with pkgs; [
            vala-language-server
            nixpkgs-fmt
          ];
        };
      });
    };
}
