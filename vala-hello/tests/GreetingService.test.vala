using GLib;

public void test_greeting_service () {
    var service = new GreetingService ("ethnarque");

    assert (service.greet () == "Hello ethnarque!");
}

public int main (string[] args) {
    Test.init (ref args);

    Test.add_func ("/test_greeting_service", test_greeting_service);
    return Test.run ();
}
