<?php
/**
 * Base production configurations goes here.
 */

/**
 * Helper function in order to retrieve variables set in $_SERVER if they exist.
 */
function env(string $key) {
    if ( array_key_exists( $key, $_ENV) ) {
        return $_ENV[$key];
    }

    return null;
}

/**
 * Directory containing all of site's files
 * @var string
**/
$root_dir = dirname(__DIR__);


if ( file_exists( $root_dir . "/.env" ) ) {
    $dotenv = Dotenv\Dotenv::createImmutable($root_dir);

    $dotenv->load();
    $dotenv->required(['WP_HOME', 'WP_SITEURL']);
    $dotenv->required(['DB_NAME', 'DB_USER', 'DB_PASSWORD']);
}
/**
 * Set up global environment constant
 */
define( 'WP_ENV', env('WP_ENV') ?: 'production' );

if ( !env('WP_ENVIRONMENT_TYPE') && in_array( WP_ENV, ['production', 'staging', 'development']) ) {
    define( 'WP_ENVIRONMENT_TYPE', WP_ENV );
}

/**
 * URLs
 */
define( 'WP_HOME', env('WP_HOME') );
define( 'WP_SITEURL', env('WP_SITEURL') );

/**
  Custom content directory
*/
define( 'CONTENT_DIR', '/app');
define( 'WP_CONTENT_DIR', $root_dir . '/web' . CONTENT_DIR);
define( 'WP_CONTENT_URL', WP_HOME . CONTENT_DIR);

/**
 * Database settings
 */
define( 'DB_NAME', env('DB_NAME') );
define( 'DB_USER', env('DB_USER') );
define( 'DB_PASSWORD', env('DB_PASSWORD') );
define( 'DB_HOST', env('DB_HOST') ?: 'localhost' );
define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );
$table_prefix = env('DB_PREFIX') ?: 'wp_';

/**
 * Authentication unique keys and salts.
 */
define( 'AUTH_KEY', env('AUTH_KEY') );
define( 'SECURE_AUTH_KEY',  env('SECURE_AUTH_KEY')  );
define( 'LOGGED_IN_KEY', env('LOGGED_IN_KEY')  );
define( 'NONCE_KEY', env('NONCE_KEY')  );
define( 'AUTH_SALT', env('AUTH_SALT')  );
define( 'SECURE_AUTH_SALT', env('SECURE_AUTH_SALT') );
define( 'LOGGED_IN_SALT', env('LOGGED_IN_SALT') );
define( 'NONCE_SALT', env('NONCE_SALT')  );

/**
 * Settings
 */
define( 'WP_DEBUG_DISPLAY', false );
define( 'WP_DEBUG_LOG', false );
define( 'SCRIPT_DEBUG', false );
ini_set( 'display_errors', '0' );

define( 'AUTOMATIC_UPDATED_DISABLED', true );
// Disable the plugin and theme file editor in the admin
define( 'DISALLOW_FILE_EDIT', true );
// Disable plugin and theme updates and installation from the admin
define( 'DISALLOW_FILE_MODS', true );

$env_cfg = __DIR__ . '/environments/' . WP_ENV . '.php';

if ( file_exists( $env_cfg ) ) {
    require_once $env_cfg;
}

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/wp' );
}

