<?php
/**
 * Configuration overrides for "development" environment.
 */

define( 'DISALLOW_INDEXING', true );
define( 'SAVEQUERIES' , true);
define( 'SCRIPT_DEBUG', true );
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true );
define( 'WP_DEBUG_LOG', env('WP_DEBUG_LOG') ?? true );
define( 'WP_DISABLE_FATAL_ERROR_HANDLER', true );
// // Enable plugin and theme updates and installation from the admin
define( 'DISALLOW_FILE_MODS', false );

ini_set( 'display_errors', '1' );
