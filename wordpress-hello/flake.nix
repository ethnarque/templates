{
  description = "Simple hello worlds using WordPress";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    devenv.url = "github:cachix/devenv";
    devenv.inputs.nixpkgs.follows = "nixpkgs";
  };

  nixConfig = {
    extra-trusted-public-keys = "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw=";
    extra-substituters = "https://devenv.cachix.org";
  };

  outputs = { self, devenv, nixpkgs } @ inputs:
    let
      systems = [ "x86_64-linux" "i686-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];

      forEachSystems = fn:
        (nixpkgs.lib.genAttrs systems
          (system: fn
            (
              let
                pkgs = import nixpkgs {
                  inherit system;
                  overlays = [ ];
                };
              in
              { inherit pkgs system; }
            )
          ));
    in
    {
      packages = forEachSystems ({ system, ... }: {
        devenv-up = self.devShells.${system}.default.config.procfileScript;
      });

      devShells = forEachSystems ({ pkgs, ... }: {
        default = devenv.lib.mkShell {
          inherit inputs pkgs;

          modules = [
            ({ pkgs, ... }: {
              name = "wordpress";

              packages = with pkgs;[
                wp-cli
                php82Packages.composer
              ];

              languages.javascript.enable = true;
              languages.php.enable = true;

              services.mysql.enable = true;
              services.mysql.package = pkgs.mariadb;
              services.mysql.initialDatabases = [{ name = "wordpress"; }];
              services.mysql.ensureUsers = [
                {
                  name = "wordpress";
                  password = "password";
                  ensurePermissions = {
                    "*.*" = "ALL PRIVILEGES";
                  };
                }
              ];

              processes.wp-server.exec = "${pkgs.wp-cli}/bin/wp server";

            })
          ];
        };
      });

      formatter = forEachSystems ({ pkgs, ... }: pkgs.nixpkgs-fmt);
    };
}
