<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the website, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// Composer autoloader
require_once dirname(__DIR__) . "/vendor/autoload.php";

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'wordpress' );

/** Database password */
define( 'DB_PASSWORD', 'password' );

/** Database hostname */
define( 'DB_HOST', '0.0.0.0' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']k#6gz,KFg&iWC]k5Wm(GQ#wI_r#DI?Fc(iQ71au8q<4oZY^^WHODs[cDU;EK}kk' );
define( 'SECURE_AUTH_KEY',  'LTXT4@6K6l$ZhPUmc2;(DQ4vesT1u9}Pb_Y95r9 *^0Of=]iD]<g}g0He;l.=s>j' );
define( 'LOGGED_IN_KEY',    'uTVP6D^?c={T[yJ*LA6,P@m}E(Zo&5B|V2L~-C/{dI]G+.LdxmoIa13@;:M$&*va' );
define( 'NONCE_KEY',        ']G8E:eJf>bFS:KUxP>2&3Dk#P-KpT`}!7f)$;h#{|K*g!_CBbF:Z5cSn#ev<Se)i' );
define( 'AUTH_SALT',        '7Hb/rNoA(C~*$bWf71>-DZzVX_1L&VlZ[iAkzrHsxtgUx7-~|RKcd2`O~;Vo@S?A' );
define( 'SECURE_AUTH_SALT', '%jx9#&TZPuu9aO^H!5<97h0Cm0+*,A~-Zjhn]b  PJ>`~V$li=h,wk491II/4F}~' );
define( 'LOGGED_IN_SALT',   '9vBNEYGTMKY`HY9K>mb1nFl#gKI+*DTdINq@~`a c-.aJKK-o<+?9/aT_!Do8E8x' );
define( 'NONCE_SALT',       '+Dc_(#&4[0:lE _D)Rvg%6%:3@MP/SRl3Xg>YX1^#tg&Z2}u~6bTuQo^=]4?f85N' );

define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/app');
define('WP_CONTENT_URL', 'http://localhost:8080/app');

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
